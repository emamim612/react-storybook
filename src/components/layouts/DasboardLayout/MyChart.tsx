import React from 'react';
import SelectBox from 'devextreme-react/select-box';
import {
  Chart,
  Series,
  ArgumentAxis,
  CommonSeriesSettings,
  CommonAxisSettings,
  Grid,
  Export,
  Legend,
  Margin,
  Tooltip,
  Label,
  Format,
} from 'devextreme-react/chart';
import { architectureSources, sharingStatisticsInfo, seriesTypeLabel } from './data';

const MyChart = ({data}: {data: any}) => {

    return (
      <React.Fragment>
        <Chart
          palette="Violet"

          dataSource={data}
        >
          <CommonSeriesSettings
            argumentField="year"
            // @ts-ignore
            type='spline'
          />
          <CommonAxisSettings>
            <Grid visible={true} />
          </CommonAxisSettings>
          {
            // @ts-ignore
            architectureSources.map((item) => <Series
              key={item.value}
              valueField={item.value}
              name={item.name} />)
          }
          <Margin bottom={20} />
          <ArgumentAxis
            allowDecimals={false}
            axisDivisionFactor={60}
          >
            <Label>
              <Format type="decimal" />
            </Label>
          </ArgumentAxis>
          <Legend
            verticalAlignment="top"
            horizontalAlignment="right"
          />
          <Tooltip enabled={true} />
        </Chart>

      </React.Fragment>
    );

}

export default MyChart;