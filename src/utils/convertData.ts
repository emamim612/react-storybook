import economicData from "src/components/layouts/DasboardLayout/economicData";

export const convertData = (dataObj: typeof economicData) => {
    try {

        const res = [] as any[]

        // Get the keys of the object excluding "year"
        const keys = Object.keys(dataObj).filter((key) => key !== "year");

        // Loop through each year and create a new object with the required structure
        dataObj["homelessness"].forEach((item, index) => {
            const obj = { year: item.year };

            keys.forEach((key) => {
                // @ts-ignore
                obj[key] = dataObj[key][index].number;
            });

            res.push(obj);
        });

        return res;
    } catch {
        return []
    }
}